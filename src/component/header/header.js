
import { Container } from "@mui/system"
import { Col, Nav, NavItem } from "react-bootstrap"
import ComponentIconNavBar from "./ComponentIconNavBar"
import ComponentLogo from "./ComponentLogo"

function Header() {
    return (
        <Container maxWidth="containerLg" className="mt-3">
            <Nav>
                <Col xs="9">
                    <NavItem>
                        <ComponentLogo></ComponentLogo>
                    </NavItem>
                </Col>
                <Col xs="3">
                    <NavItem>
                        <ComponentIconNavBar></ComponentIconNavBar>
                    </NavItem>
                </Col>
            </Nav>
        </Container>
    )
}
export default Header