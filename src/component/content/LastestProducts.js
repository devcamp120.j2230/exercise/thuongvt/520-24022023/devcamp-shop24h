import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography } from "@mui/material"
import { Container } from "@mui/system"
import imgCarousel1 from "../../assetment/image/JBL Endurance Sprint-products-8.jpg";
import img1 from "../../assetment/image/JBL E55BT Key Black-products-2.jpg";
import img2 from "../../assetment/image/JBL Quantum 100-carousel-1.jpg";
import img3 from "../../assetment/image/JBL Quantum 400-products-1.png";
import img4 from "../../assetment/image/JBL T510BT-carousel-2.jpg";
import img5 from "../../assetment/image/JBL Tune 220TWS-products-6.jpg";
import img6 from "../../assetment/image/JBL Tune 750BTNC red-products-4.jpg";
import img7 from "../../assetment/image/JBL UA Project Rock-products-7 .jpg";


function LastestProducts() {
    return (
        <>
            <Container className="text-center">
                <Grid className="latest-product">
                    <Typography style={{ fontSize: "35px", fontWeight: 900 }}>LATEST PRODUCT</Typography>
                </Grid>
            </Container>
            <Container className="mt-5 text-center" style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}  >
                <Grid item spacing={3}>
                    <Grid item xs={12} sm={12} md={12} lg={3}>
                        <Card sx={{ maxWidth: 150 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                            <CardActionArea>
                                <CardMedia component="img"
                                    image={imgCarousel1}
                                    height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{ marginTop: "10px", fontSize: "10px" }}>
                                        JBL Quantum 400
                                    </Typography>
                                    <Typography >
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item spacing={3}>
                    <Grid item xs={12} sm={12} md={12} lg={3}>
                        <Card sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                            <CardActionArea>
                                <CardMedia component="img"
                                    image={img1}
                                    height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{ marginTop: "10px", fontSize: "10px" }}>
                                        JBL Quantum 400
                                    </Typography>
                                    <Typography >
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item spacing={3}>
                    <Grid item xs={12} sm={12} md={12} lg={3}>
                        <Card sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                            <CardActionArea>
                                <CardMedia component="img"
                                    image={img2}
                                    height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{ marginTop: "10px", fontSize: "10px" }}>
                                        JBL Quantum 400
                                    </Typography>
                                    <Typography >
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item spacing={3}>
                    <Grid item xs={12} sm={12} md={12} lg={3}>
                        <Card sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                            <CardActionArea>
                                <CardMedia component="img"
                                    image={img3}
                                    height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{ marginTop: "10px", fontSize: "10px" }}>
                                        JBL Quantum 400
                                    </Typography>
                                    <Typography >
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item spacing={3}>
                    <Grid item xs={12} sm={12} md={12} lg={3}>
                        <Card sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                            <CardActionArea>
                                <CardMedia component="img"
                                    image={img4}
                                    height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{ marginTop: "10px", fontSize: "10px" }}>
                                        JBL Quantum 400
                                    </Typography>
                                    <Typography >
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item spacing={3}>
                    <Grid item xs={12} sm={12} md={12} lg={3}>
                        <Card sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                            <CardActionArea>
                                <CardMedia component="img"
                                    image={img5}
                                    height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{ marginTop: "10px", fontSize: "10px" }}>
                                        JBL Quantum 400
                                    </Typography>
                                    <Typography >
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item spacing={3}>
                    <Grid item xs={12} sm={12} md={12} lg={3}>
                        <Card sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                            <CardActionArea>
                                <CardMedia component="img"
                                    image={img6}
                                    height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{ marginTop: "10px", fontSize: "10px" }}>
                                        JBL Quantum 400
                                    </Typography>
                                    <Typography >
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item spacing={3}>
                    <Grid item xs={12} sm={12} md={12} lg={3}>
                        <Card sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                            <CardActionArea>
                                <CardMedia component="img"
                                    image={imgCarousel1}
                                    height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{ marginTop: "10px", fontSize: "10px" }}>
                                        JBL Quantum 400
                                    </Typography>
                                    <Typography >
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item spacing={3}>
                    <Grid item xs={12} sm={12} md={12} lg={3}>
                        <Card sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                            <CardActionArea>
                                <CardMedia component="img"
                                    image={img7}
                                    height="250px">
                                </CardMedia>
                                <CardContent>
                                    <Typography style={{ marginTop: "10px", fontSize: "10px" }}>
                                        JBL Quantum 400
                                    </Typography>
                                    <Typography >
                                        <del>$300</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$200</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}
export default LastestProducts